package fr.arln.android

import com.goterl.lazycode.lazysodium.utils.Key
import java.util.*

class Identity {

    var ip: String = "192.168.1.123" // TODO detect local IP
    var port: Int = 8008

    private val keyPair = Utils.getKeyPair()
    private val secretKeyCurve25519 = Utils.convertKeyToCurve25519(keyPair.secretKey)

    fun present(): String {
        val publicKey = keyPair.publicKey.asBytes
        val encodedKey = Base64.getEncoder().encodeToString(publicKey)

        return "@$encodedKey.ed25519"
    }

    fun getPublicKey(): Key = keyPair.publicKey

    fun signDetached(message: String) = Utils.signDetached(message, keyPair.secretKey)

    fun scalarMult(key: Key) = Utils.scalarMult(key, secretKeyCurve25519)
}