package fr.arln.android

import com.goterl.lazycode.lazysodium.LazySodiumJava
import com.goterl.lazycode.lazysodium.SodiumJava
import com.goterl.lazycode.lazysodium.interfaces.*
import com.goterl.lazycode.lazysodium.utils.Key
import com.goterl.lazycode.lazysodium.utils.KeyPair

object Utils {
    private val lazySodium = LazySodiumJava(SodiumJava())
    private val auth: Auth.Lazy = lazySodium
    private val dh: DiffieHellman.Lazy = lazySodium
    private val hash: Hash.Lazy = lazySodium
    private val sb: SecretBox.Lazy = lazySodium
    private val sign: Sign.Lazy = lazySodium
    private val signNative: Sign.Native = lazySodium

    private const val nonceSize = 24

    @JvmStatic
    fun convertKeyToCurve25519(key: Key): Key {
        val keyCurve = ByteArray(Sign.CURVE25519_PUBLICKEYBYTES)
        val converted = signNative.convertPublicKeyEd25519ToCurve25519(keyCurve, key.asBytes)

        if (!converted) {
            throw Exception("Failed to convert the server public key")
        }

        return Key.fromBytes(keyCurve)
    }

    @JvmStatic
    fun hmac(message: String, key: Key) =
        auth.cryptoAuthHMACSha(Auth.Type.SHA512256, message, key).toByteArray()

    @JvmStatic
    fun hmacVerify(authenticator: String, message: String, key: Key) =
        auth.cryptoAuthHMACShaVerify(Auth.Type.SHA512256, authenticator, message, key)

    @JvmStatic
    fun getKeyPair(): KeyPair = sign.cryptoSignKeypair()

    @JvmStatic
    fun signDetached(message: String, secretKey: Key): String = sign.cryptoSignDetached(message, secretKey)

    @JvmStatic
    fun verifyDetached(signature: String, message: String, publicKey: Key) = sign.cryptoSignVerifyDetached(
        signature, message, publicKey
    )

    @JvmStatic
    fun scalarMult(publicKey: Key, secretKey: Key): Key = dh.cryptoScalarMult(publicKey, secretKey)

    @JvmStatic
    fun hash(string: String): String = hash.cryptoHashSha256(string)

    @JvmStatic
    fun secretBox(message: String, key: Key): String = sb.cryptoSecretBoxEasy(
        message,
        ByteArray(nonceSize) { 0 },
        key
    )

    @JvmStatic
    fun openSecretBox(cipher: String, key: Key): String = sb.cryptoSecretBoxOpenEasy(
        cipher,
        ByteArray(nonceSize) { 0 },
        key
    )
}