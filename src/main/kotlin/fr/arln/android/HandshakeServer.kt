package fr.arln.android

import com.goterl.lazycode.lazysodium.utils.Key
import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import kotlinx.coroutines.Dispatchers
import java.net.InetSocketAddress

class HandshakeServer(private val network: Network, private val server: Identity, private val client: Client) {

    private companion object {
        const val clientHelloSize = 64
        const val clientAuthenticationSize = 96
    }

    private val keyPair = Utils.getKeyPair()

    private fun verifyClientHello(message: ByteArray): Key {
        if (message.size != clientHelloSize) {
            throw Exception("""Incorrect client hello message length: ${message.size} (instead of ${clientHelloSize})""")
        }

        val clientHmac = message.sliceArray(0..31)
        val publicKey = Key.fromBytes(message.sliceArray(32..63))
        val verified = Utils.hmacVerify(clientHmac.toString(), publicKey.asPlainString, network.identifier)

        if (verified) {
            return publicKey
        }

        throw Exception("Client hello message could not be verified")
    }

    private fun helloMessage() =
        Utils.hmac(keyPair.publicKey.asPlainString, network.identifier) + keyPair.publicKey.asBytes

    private fun deriveFirstSharedSecrets(ephemeralClientKey: Key): Pair<Key, Key> {
        val sharedSecretab = Utils.scalarMult(ephemeralClientKey, keyPair.secretKey)
        val sharedSecretaB = server.scalarMult(ephemeralClientKey)

        return Pair(sharedSecretab, sharedSecretaB)
    }

    private fun clientAuthenticationKey(firstSecret: Key, secondSecret: Key): String {
        val key = network.identifier.asPlainString + firstSecret.asPlainString + secondSecret.asPlainString
        return Utils.hash(key)
    }

    private fun verifyClientAuthentication(message: ByteArray, firstSecret: Key, secondSecret: Key): Pair<ByteArray, Key> {
        val plainMessage = Utils.openSecretBox(
            message.toString(),
            Key.fromPlainString(clientAuthenticationKey(firstSecret, secondSecret))
        ).toByteArray()

        if (plainMessage.size != clientAuthenticationSize) {
            throw Exception("""Incorrect client authentication message length: ${message.size} (instead of ${clientAuthenticationSize})""")
        }

        val detachedSignatureA = message.sliceArray(0..63)
        val clientPublicKey = Key.fromBytes(message.sliceArray(64..95))
        val payload = network.identifier.asPlainString + server.getPublicKey().asPlainString +
                Utils.hash(firstSecret.asPlainString)
        val verified = Utils.verifyDetached(
            detachedSignatureA.toString(),
            payload,
            clientPublicKey
        )

        if (verified) {
            return Pair(detachedSignatureA, clientPublicKey)
        }

        throw Exception("Client authentication verification failed")
    }

    private fun thirdSecret(clientPublicKey: Key): Key {
        val clientKeyCurve = Utils.convertKeyToCurve25519(clientPublicKey)
        return Utils.scalarMult(clientKeyCurve, keyPair.secretKey)
    }

    private fun detachedSignatureB(detachedSignatureA: String, sharedSecrets: SharedSecrets, clientPublicKey: Key): String {
        val message = network.identifier.asPlainString + detachedSignatureA +
                clientPublicKey.asPlainString + Utils.hash(sharedSecrets.firstSecret.asPlainString)
        return server.signDetached(message)
    }

    private fun authenticationKey(sharedSecrets: SharedSecrets): String {
        val key = network.identifier.asPlainString + sharedSecrets.firstSecret.asPlainString +
                sharedSecrets.secondSecret.asPlainString + sharedSecrets.thirdSecret.asPlainString
        return Utils.hash(key)
    }

    private fun authenticationMessage(detachedSignatureA: String, sharedSecrets: SharedSecrets, clientPublicKey: Key): String {
        return Utils.secretBox(
            detachedSignatureB(detachedSignatureA, sharedSecrets, clientPublicKey),
            Key.fromPlainString(authenticationKey(sharedSecrets))
        )
    }

    suspend fun proceed(): SharedSecrets {
        val clientSocket = aSocket(ActorSelectorManager(Dispatchers.IO)).tcp()
            .connect(InetSocketAddress(client.ip, client.port))
        val serverSocket = aSocket(ActorSelectorManager(Dispatchers.IO)).tcp()
            .connect(InetSocketAddress(server.ip, server.port))
        val input = clientSocket.openReadChannel()
        val output = serverSocket.openWriteChannel(autoFlush = true)

        val clientHello = ByteArray(clientHelloSize)
        input.readFully(clientHello, 0, clientHelloSize)

        val helloMessage = helloMessage()
        output.writeFully(helloMessage, 0, helloMessage.size)

        val clientAuthentication = ByteArray(clientAuthenticationSize)
        input.readFully(clientAuthentication, 0, clientAuthenticationSize)

        val ephemeralClientKey = verifyClientHello(clientHello)
        val (firstSecret, secondSecret) = deriveFirstSharedSecrets(ephemeralClientKey)
        val (detachedSignatureA, clientPublicKey) = verifyClientAuthentication(clientAuthentication, firstSecret, secondSecret)
        val sharedSecrets = SharedSecrets(firstSecret, secondSecret, thirdSecret(clientPublicKey))

        val authenticationMessage = authenticationMessage(detachedSignatureA.toString(), sharedSecrets, clientPublicKey).toByteArray()
        output.writeFully(authenticationMessage, 0, authenticationMessage.size)

        return sharedSecrets
    }
}
