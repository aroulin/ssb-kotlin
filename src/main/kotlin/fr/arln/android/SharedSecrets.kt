package fr.arln.android

import com.goterl.lazycode.lazysodium.utils.Key

class SharedSecrets(val firstSecret: Key, val secondSecret: Key, val thirdSecret: Key)