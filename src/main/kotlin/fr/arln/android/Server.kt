package fr.arln.android

import com.goterl.lazycode.lazysodium.utils.Key

class Server(val publicKey: Key, val ip: String, val port: Int)