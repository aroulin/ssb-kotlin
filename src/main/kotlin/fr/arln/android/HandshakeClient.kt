package fr.arln.android

import com.goterl.lazycode.lazysodium.utils.Key
import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import kotlinx.coroutines.Dispatchers
import java.net.InetSocketAddress

class HandshakeClient(private val network: Network, private val client: Identity, private val server: Server) {

    // TODO share constants with server
    private companion object {
        const val serverHelloSize = 64
        const val serverAuthenticationSize = 80
    }

    private val keyPair = Utils.getKeyPair()

    private fun helloMessage() =
        Utils.hmac(keyPair.publicKey.asPlainString, network.identifier) + keyPair.publicKey.asBytes

    private fun verifyServerHello(message: ByteArray): Key {
        if (message.size != serverHelloSize) {
            throw Exception("""Incorrect server hello message length: ${message.size} (instead of $serverHelloSize)""")
        }

        val serverHmac = message.sliceArray(0..31)
        val publicKey = Key.fromBytes(message.sliceArray(32..63))
        val verified = Utils.hmacVerify(
            serverHmac.toString(),
            publicKey.asPlainString,
            network.identifier
        )

        if (verified) {
            return publicKey
        }

        throw Exception("Server hello message could not be verified")
    }

    private fun deriveSharedSecrets(ephemeralServerKey: Key): SharedSecrets {
        val serverKeyCurve = Utils.convertKeyToCurve25519(server.publicKey)

        val sharedSecretab = Utils.scalarMult(ephemeralServerKey, keyPair.secretKey)
        val sharedSecretaB = Utils.scalarMult(serverKeyCurve, keyPair.secretKey)
        val sharedSecretAb = client.scalarMult(ephemeralServerKey)

        return SharedSecrets(sharedSecretab, sharedSecretaB, sharedSecretAb)
    }

    private fun detachedSignatureA(sharedSecretab: Key): String {
        val message = network.identifier.asBytes +
                server.publicKey.asBytes +
                Utils.hash(sharedSecretab.asPlainString).toByteArray()
        return client.signDetached(message.toString())
    }

    private fun authenticationPayload(sharedSecretab: Key): String {
        return detachedSignatureA(sharedSecretab) + client.getPublicKey().asPlainString
    }

    private fun authenticationKey(sharedSecrets: SharedSecrets): String {
        val key = network.identifier.asPlainString + sharedSecrets.firstSecret.asPlainString +
                sharedSecrets.secondSecret.asPlainString
        return Utils.hash(key)
    }

    private fun authenticationMessage(sharedSecrets: SharedSecrets): String {
        return Utils.secretBox(
            authenticationPayload(sharedSecrets.firstSecret),
            Key.fromPlainString(authenticationKey(sharedSecrets))
        )
    }

    private fun serverAuthenticationKey(sharedSecrets: SharedSecrets): Key {
        val key = network.identifier.asPlainString + sharedSecrets.firstSecret.asPlainString +
                sharedSecrets.secondSecret.asPlainString + sharedSecrets.thirdSecret.asPlainString
        return Key.fromPlainString(Utils.hash(key))
    }

    private fun detachedSignatureB(message: ByteArray, sharedSecrets: SharedSecrets) =
        Utils.openSecretBox(
            message.toString(),
            serverAuthenticationKey(sharedSecrets)
        )

    private fun verifyServerAuthentication(message: ByteArray, sharedSecrets: SharedSecrets): Boolean {
        val detachedSignatureA = detachedSignatureA(sharedSecrets.firstSecret)
        val detachedSignatureB = detachedSignatureB(message, sharedSecrets)

        val payload = network.identifier.asPlainString + detachedSignatureA + client.getPublicKey().asPlainString +
                Utils.hash(sharedSecrets.firstSecret.asPlainString)

        return Utils.verifyDetached(detachedSignatureB, payload, server.publicKey)
    }

    suspend fun proceed(): SharedSecrets {
        val clientSocket = aSocket(ActorSelectorManager(Dispatchers.IO)).tcp()
            .connect(InetSocketAddress(client.ip, client.port))
        val serverSocket = aSocket(ActorSelectorManager(Dispatchers.IO)).tcp()
            .connect(InetSocketAddress(server.ip, server.port))
        val input = clientSocket.openReadChannel()
        val output = serverSocket.openWriteChannel(autoFlush = true)

        val helloMessage = helloMessage()
        output.writeFully(helloMessage, 0, helloMessage.size)

        val serverHello = ByteArray(serverHelloSize)
        input.readFully(serverHello, 0, serverHelloSize)

        val ephemeralServerKey = verifyServerHello(serverHello)
        val sharedSecrets = deriveSharedSecrets(ephemeralServerKey)
        val authenticationMessage = authenticationMessage(sharedSecrets).toByteArray()

        output.writeFully(authenticationMessage, 0, authenticationMessage.size)

        val serverAuthentication = ByteArray(serverAuthenticationSize)
        input.readFully(serverAuthentication, 0, serverAuthenticationSize)

        if (verifyServerAuthentication(serverAuthentication, sharedSecrets)) {
            return sharedSecrets
        }

        throw Exception("Server authentication verification failed")
    }
}